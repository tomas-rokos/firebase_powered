library firebase_powered;

import "dart:async";
import "dart:convert";
import "dart:collection";

import "general.dart" as DataSource;
import "map_delta.dart";

part "impl/db_ref.dart";
part "impl/db_local_cache.dart";

const String defaultAppName = "[DEFAULT]";

class QueryProps {
	List<String> keyElements = [];
	String get key {
		if (keyElements == null)
			return null;
		List<String> res = [];
		keyElements.forEach((String elem) {
			if (elem == null) {
				if (res.isNotEmpty) {
					res.removeLast();
				}
			} else {
				res.add(elem);
			}
		});

		return res.isEmpty ? null : res.join("/");
	}
}

abstract class DbRef {
	///////// key, value - data representation
	String get key;
	dynamic get value;
	Stream<dynamic> get onValue;
	void onValueCancelAll();
	/////// database walker
	DbRef get root;
	DbRef get parent;
	DbRef child(String path);
	/////// variables
	String getVar(String varName);
	void setVar(String varName, String varValue);
	DbRef childViaVar(String varName,[String nestedPath]);
	DbRef as(String recordName);
//	//////// complex value
//	Map<String,dynamic> get complexValue;
//	Stream get onComplexValue;
	Future waitForRemoteValue();
}

abstract class DatabaseProvider {
	DatabaseProvider(String appName) {
		database(appName)._cache.remote = this;
	}
	StreamSubscription subscribe(QueryProps qry,void onData(String path, dynamic val));
	Future update(Map<String,dynamic> vals);
}

////////////////////////////////////////////////////////////////////////////////

class Database {
	final String appName;
	_LocalCache _cache = new _LocalCache();

	Database(this.appName) {
	}

	DbRef ref([String path]) {
		_DbRefImpl s = new _DbRefStart(this);
		return path == null ? s : new _DbRefChild(s, path);
	}
	void update(Map<String,dynamic> vals) {
		_cache.update(vals);
	}
	void setLocalData(String path, dynamic val) {
		if (path == null)
			_cache.data = new DataSource.General(new SplayTreeMap.from(val));
		else
			_cache.update({path:val},localUpdate: false);
	}
}

Database database([String app=defaultAppName]) => _Abstractor.instance.database(app);

////////////////////////////////////////////////////////////////////////////////

class AuthProvider {}
class GoogleAuthProvider extends AuthProvider {}

class User {
	String displayName;
	String email;
	String uid;
}

class Auth {
	final String appName;
	Auth(this.appName) {
		_Abstractor.instance._auths[appName] = this;
	}
	Future<dynamic> signOut() => new Future.value(true);
	Future<dynamic> signInWithRedirect(AuthProvider provider) => new Future.value(true);
	final StreamController _onAuthStateChangedSC = new StreamController.broadcast();
	Stream<User> get onAuthStateChanged {
		return _onAuthStateChangedSC.stream;
	}
	User _currUser = null;
	User get currentUser => _currUser;
	set currentUser(User us) {
		_currUser = us;
		_onAuthStateChangedSC.add(_currUser);
	}
}

Auth auth([String app=defaultAppName]) => _Abstractor.instance.auth(app);

////////////////////////////////////////////////////////////////////////////////

class _Abstractor {
	static _Abstractor _instance;
	static _Abstractor get instance {
		if (_Abstractor._instance == null)
			_Abstractor._instance = new _Abstractor();
		return _Abstractor._instance;
	}
	Map<String,Database> _dbs = {};
	Database database(String app) {
		if (_dbs.containsKey(app) == false) {
			_dbs[app] = new Database(app);
		}
		return _dbs[app];
	}
	Map<String,Auth> _auths = {};
	Auth auth(String app) {
		if (_auths.containsKey(app) == false) {
			new Auth(app);
		}
		return _auths[app];
	}
}