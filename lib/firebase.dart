import "dart:async";

import "package:firebase/firebase.dart" as Firebase;

import "firebase_powered.dart" as fp;

class DatabaseProviderFirebase extends fp.DatabaseProvider {
	String _app;
	DatabaseProviderFirebase(this._app):super(_app);
	StreamSubscription subscribe(fp.QueryProps qry, void onData(String path, dynamic val)) {
		return Firebase.database(Firebase.app(_app)).ref(qry.key).onValue.listen((Firebase.QueryEvent qe) {
			String fullKey = "";
			Firebase.DatabaseReference ref = qe.snapshot.ref;
			while(ref.key != null) {
				fullKey = fullKey.isEmpty ? ref.key : ref.key + "/" + fullKey;
				ref = ref.parent;
			}
			onData(fullKey, qe.snapshot.val());
		});
	}
	Future update(Map<String,dynamic> vals) {
		return Firebase.database(Firebase.app(_app)).ref("/").update(vals);
	}
}

class AuthFirebase extends fp.Auth {
	AuthFirebase(String appName):super(appName);

	Future<dynamic> signOut() => Firebase.auth(Firebase.app(appName)).signOut();
	Future<dynamic> signInWithRedirect(fp.AuthProvider provider) {
		if (provider is fp.GoogleAuthProvider)
			return Firebase.auth(Firebase.app(appName)).signInWithRedirect(new Firebase.GoogleAuthProvider());
		else
			return new Future.value(true);
	}
	Stream<fp.User> get onAuthStateChanged {
		Firebase.auth(Firebase.app(appName)).onAuthStateChanged.listen((Firebase.User us) {
			fp.User u = null;
			if (us != null) {
				u = new fp.User();
				u.displayName = us.displayName;
				u.email = us.email;
				u.uid = us.uid;
			}
			currentUser = u;
		});
		return super.onAuthStateChanged;
	}

}

void initializeApp({String apiKey, String authDomain, String databaseURL, String storageBucket, String appName=fp.defaultAppName}) {
	Firebase.initializeApp(
		apiKey: apiKey,
		authDomain: authDomain,
		databaseURL: databaseURL,
		storageBucket: storageBucket,
		name: appName);
	new DatabaseProviderFirebase(appName);
	new AuthFirebase(appName);
}
