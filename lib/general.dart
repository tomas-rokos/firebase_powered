import 'dart:convert';

class General {
	final dynamic _v;
	General(this._v);
	dynamic operator[](String key) {
		if (key == null)
			return _v;
		List<String> path = key.split("/");
		return _child(path);
	}
	operator[]=(String key,dynamic newVal) {
		if (key == null) {
			new Exception("cannot set THE value");
			return;
		}
		List<String> path = key.split("/");
		_childSet(path, newVal);
	}
	String asString([String key,String def=""]) {
		List<String> path = key != null ? key.split("/") : [];
		dynamic val = _child(path);
		if(val is Map) {
			try {
				String v = JSON.encode(val);
				return v != null ? v : def;
			}
			catch(e){
				return def;
			}
		}
		return val != null ? val.toString() : def;
	}
	int asInt([String key, int def=0]) {
		String v = asString(key);
		return double.parse(v,(_) => def.toDouble()).toInt();
	}
	double asDouble([String key, double def=0.0]) {
		String v = asString(key);
		return double.parse(v,(_) => def);
	}
	DateTime asDateTime([String key, DateTime def=null]) {
		String v = asString(key);
		if(v == ""){ return def; };
		try {
			return DateTime.parse(v);
		}
		catch(e){
			return def;
		}
	}
	Map asMap([String key, Map def=const {}]) {
		List<String> path = key != null ? key.split("/") : [];
		dynamic val = _child(path);

		if(val is String) {
			try {
				Map v = JSON.decode(val) as Map;
				return v != null ? v : def;
			}
			catch(e){
				return def;
			}
		}
		else {
			try {
				Map v = val as Map;
				return v != null ? v : def;
			}
			catch(e){
				return def;
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////
	//
	//  private
	//
	dynamic _child(List<String> path) {
		dynamic c = _v;
		int len = path.length;
		for (int i=0; i<len; ++i) {
			String curPathElem = path[i];
			if (curPathElem.isEmpty)
				return c;
			if (!(c is Map))
				return null;
			Map cm = c;
			dynamic v = cm[curPathElem];
			if (v == null)
				return null;
			c = v;
		}
		return c;
	}

	void _childSet(List<String> path,dynamic newVal) {
		dynamic finalVal = newVal;
		if (newVal is DateTime) {
			finalVal = newVal.toIso8601String();
		}
		if (path.length == 0) {
			new Exception("general cannot change THE value");
			return;
		}
		if (!(_v is Map)) {
			new Exception("general needs to wrap MAP to set nested elements");
			return;
		}
		Map c = _v;
		List<Map> nestedMaps = [c];
		int len = path.length;
		for (int i=0; i<len-1; ++i) {
			if (c.containsKey(path[i])) {
				dynamic cd = c[path[i]];
				if (!(cd is Map)) {
					c[path[i]] = new Map();
				}
			} else {
				c[path[i]] = new Map();
			}
			c = c[path[i]];
			nestedMaps.add(c);
		}
		if (finalVal != null)
			c[path[len-1]] = finalVal;
		else {
			for (int i=len-1;i>=0;--i) {
				int nmi = i;
				nestedMaps[nmi].remove(path[i]);
				if (nestedMaps[nmi].keys.isNotEmpty)
					break;

			}
		}
	}
}