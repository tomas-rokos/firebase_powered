import "general.dart";

class MapDelta {
	static Map<String,dynamic> execute(Map theMap, Map base) {
		Map<String, dynamic> ret = {};

		General theMapGeneral = new General(theMap);
		List<String> theMapKeys = _keys(theMap);
		General baseGeneral = new General(base);
		List<String> baseKeys = _keys(base);

		List<String> toAdd = _minus(theMapKeys,baseKeys);
		toAdd.forEach((String k) {
			ret[k] = theMapGeneral[k];
		});

		List<String> toRemove = _minus(baseKeys,theMapKeys);
		toRemove.forEach((String k) {
			ret[k] = null;
		});
		List<String> toCheck = _minus(theMapKeys, toAdd);
		toCheck.forEach((String k) {
			String s1 = theMapGeneral.asString(k);
			String s2 = baseGeneral.asString(k);
			if (s1.compareTo(s2)!=0)
				ret[k] = theMapGeneral[k];
		});

		return ret;
	}
	static List<String> _keys(Map m,[String prefix=""]) {
		if (m == null)
			return [];
		List<String> ret = [];

		m.keys.forEach((dynamic k) {
			String k_s = k.toString();
			if (m[k] is Map) {
				ret.addAll(_keys(m[k],prefix+k_s+"/"));
			} else {
				ret.add(prefix+k_s);
			}
		});
		return ret;
	}
	static List<String> _minus(List<String> base, List<String> substract) {
		List<String> ret = [];
		base.forEach((String s) {
			if (substract.contains(s) == false)
				ret.add(s);
		});
		return ret;
	}
}