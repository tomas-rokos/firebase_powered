part of firebase_powered;

////////////////////////////////////////////////////////////////////////////////

class _DbRefImpl extends DbRef {
	_DbRefImpl _parentImpl;
	_DbRefImpl(this._parentImpl) {
		start.onInvalidate.listen((_) {
			if (_scs.isNotEmpty)
				_connect();
		});
	}

	String get key => queryProps.key;
	dynamic get value => start.db._cache.value(queryProps);

	List<StreamController> _scs = [];
	Stream<dynamic> get onValue {
		StreamController sc = new StreamController();
		new Future.value(0).then((_) => sc.add(value));
		sc.onCancel = () {
			sc.close();
			_scs.remove(sc);
			if (_scs.isEmpty)
				_stopListening();
		};
		_scs.add(sc);
		_connect(sendUpdates: false);
		return sc.stream;
	}
	DbRef get root => new _DbRefChild(this,null);
	DbRef get parent => new _DbRefParent(this);
	DbRef child(String path) => new _DbRefChild(this,path);

	String getVar(String varName) => start.vars[varName];
	void setVar(String varName, String varValue) => start.setVar(varName,varValue);
	DbRef childViaVar(String varName,[String nestedPath]) => new _DbRefChildViaVar(this, varName, nestedPath);

	DbRef as(String recordName) => new _DbRefChildAs(this, recordName);

	void onValueCancelAll() {
		_stopListening();
		_scs.forEach((StreamController sc) => sc.close());
		_scs.clear();
	}

	Future<dynamic> waitForRemoteValue() {
		QueryProps qry = queryProps;
		if (qry.key == null)
			return new Future.value(null);
		return start.db._cache.connectToRemote(qry);

	}
//	Map<String,dynamic> get complexValue => start.complex;
//	Stream get onComplexValue => start._onComplexValueSC.stream;

	//////////////-- private support methods --/////////////////////
	_DbRefStart get start {
		_DbRefImpl impl = this;
		while (impl._parentImpl != null)
			impl = impl._parentImpl;
		if (impl is _DbRefStart)
			return impl;
		new Exception("First impl in chain must be _DbRefStart");
		return null;
	}

	StreamSubscription _subscr;
	String _subscrKey = null;

	void _connect({bool sendUpdates = true}) {
		QueryProps qry = queryProps;
		String newKey = qry.key;
		if (newKey == _subscrKey)
			return;
		_stopListening();
		_subscrKey = newKey;
		if (sendUpdates) {
			dynamic val = value;
			_scs.forEach((StreamController sc) => sc.add(val));
		}
		if (_subscrKey == null)
			return;
		_subscr = start.db._cache.subscribe(qry).listen((dynamic val) {
			_scs.forEach((StreamController sc) => sc.add(val));
		});
	}
	void _stopListening() {
		_subscr?.cancel();
		_subscr = null;
		_subscrKey = null;
	}
	QueryProps get queryProps {
		QueryProps q = new QueryProps();
		calc(q);
		return q;
	}
	void calc(QueryProps qry) {
		_parentImpl?.calc(qry);
	}
}

////////////////////////////////////////////////////////////////////////////////

class _DbRefStart extends _DbRefImpl {
	Database db;
	Map<String,String> vars = {};
	_DbRefStart(this.db):super(null);

	void setVar(String varName,String varValue) {
		vars[varName] = varValue;
		_onInvalidateSC.add(varName);
	}

	final StreamController _onInvalidateSC = new StreamController.broadcast();
	Stream get onInvalidate => _onInvalidateSC.stream;

	Map<String,dynamic> complex = {};
//	final StreamController _onComplexValueSC = new StreamController.broadcast();
//
//	void setComplexValue(String name, dynamic val) {
//		complex[name] = val;
//		_onInvalidateSC.add(name);
//		_onComplexValueSC.add(complexValue);
//	}
}

class _DbRefChild extends _DbRefImpl {
	String nodeName;
	_DbRefChild(_DbRefImpl p,this.nodeName):super(p);
	void calc(QueryProps qry) {
		// setting root, no _parentImpl iteration
		if (nodeName == null) {
			return;
		}
		qry.keyElements.insert(0, nodeName);
		super.calc(qry);
	}
}

class _DbRefParent extends _DbRefImpl {
	_DbRefParent(_DbRefImpl p):super(p);
	void calc(QueryProps qry) {
		qry.keyElements.insert(0, null);
		super.calc(qry);
	}
}

class _DbRefChildViaVar extends _DbRefImpl {
	String varName;
	String nestedPath;
	_DbRefChildViaVar(_DbRefImpl p,this.varName, this.nestedPath):super(p);
	void calc(QueryProps qry) {
		DataSource.General cpl = new DataSource.General(start.complex);
		String chld = cpl.asString(nestedPath != null ? varName+"/"+nestedPath : varName,null);
		if (chld == null) {
			chld = start.vars[varName];
			if (chld == null) {
				qry.keyElements = null;
				return;
			}
		}
		qry.keyElements.insert(0, chld);
		super.calc(qry);
	}
}

class _DbRefChildAs extends _DbRefImpl {
	String recordName;
	_DbRefChildAs(_DbRefImpl p,this.recordName):super(p) {
//		p.onValue.listen((dynamic v) {
//			start.setComplexValue(recordName, v);
//		});
	}
}


