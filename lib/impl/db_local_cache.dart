part of firebase_powered;

class _LocalCache {
	DataSource.General data = new DataSource.General(new SplayTreeMap());
	DatabaseProvider remote = null;

	List<String> _remotes = [];
	Future<dynamic> connectToRemote(QueryProps qry) {
		String k = qry.key;
		if (remote == null)
			return new Future.value(data[k]);
		if (_remotes.firstWhere((String s) => k.indexOf(s) != -1,orElse: () => null) != null)
			return new Future.value(data[k]);
		Completer c = new Completer();
		_remotes.add(qry.key);
		remote.subscribe(qry, (String path, dynamic val) {
			update({path:val},localUpdate: true);
			if (c.isCompleted == false)
				c.complete(val);
		});
		return c.future;
	}

	dynamic value(QueryProps qry) {
		String key = qry.key;
		if (key == null)
			return null;
		connectToRemote(qry);
		return data[key];
	}
	List<Map> _subs = [];
	Stream subscribe(QueryProps qry) {
		connectToRemote(qry);

		StreamController sc = new StreamController();
		sc.onCancel = () {
			sc.close();
			Map mp =_subs.firstWhere((Map m) => m["s"] == sc);
			_subs.remove(mp);
		};
		_subs.add({"s":sc,"q":qry});
		return sc.stream;
	}
	void update(Map<String,dynamic> vals,{bool localUpdate=false}) {
		List<Map> toSend = [];
		vals.forEach((String k,dynamic v) {
			if (v is Map) {
				if (MapDelta.execute(v, data[k]).isEmpty)
					return;
			} else {
				if (v == data[k])
					return;
			}

			_subs.forEach((Map m) {
				if ((k.indexOf(m["q"].key) == 0 || m["q"].key.indexOf(k) == 0) && toSend.contains(m)==false) {
					toSend.add(m);
				}
			});
			data[k]=v;
		});
		_splayMap(data[null]);
		toSend.forEach((Map m) {
			m["s"].add(JSON.decode(JSON.encode(data[m["q"].key])));
		});
		if (localUpdate == false)
			remote?.update(vals);
	}
	void _splayMap(Map mp) {
		mp.forEach((String key, dynamic val) {
			if (val is SplayTreeMap) {
				_splayMap(val);
				return;
			}
			if (!(val is Map))
				return;
			SplayTreeMap tmap = new SplayTreeMap.from(val);
			mp[key] = tmap;
			_splayMap(tmap);
		});
	}
}