# Firebase Powered
Library with extension to official <https://pub.dartlang.org/packages/firebase>. I would like to stay as close as
possible to official API, so initial cost to use this library is almost zero. 
Add this library <https://bitbucket.org/tomas-rokos/firebase_powered> to your project and just change import from

`import "package:firebase/firebase.dart";`

to

`import "package:firebase_powered/firebase_powered.dart";`

## Playground
Library has testing data and sample queries deployed at <https://playground-a8df2.firebaseapp.com>

## Database
Database reference is constructed by calling `database().ref()`. 

### Local database
Firebase_powered uses local snapshot to serve data. This snapshot is built by observing what client needs 
and keep data up to date from server wihtout client explicit touch. It is possible to add purely local data, 
they will become part of snaphot without the need
to download from server. It is also possible to use firebase_powered without connecting to firebase server.     
This is useful for two major workflows:

* reducing database traffic - Portion of the database, which is more less static, could be downloaded from CDN as JSON and feeded into your application, wihtout changing any query. So you might start with everything in server database and then decide and take a portion out and store it in a file.
* using in tests - you could create tests which will use firebase database data, but you don't need to call `initializeApp`, but only provide snapshot of database, usually subset, which is necessary for testing to be on real data. Those tests will then not make a traffic to server database and will be resistant to any data changes in server database.


### Dynamic queries
Variables help to have prepared query and just change one or more of elements, when all listeners and instances remains
 the same. Property `value` and listeners `onValue` start to provide data from new database location.
```
DbRef ref = database().ref().child("a").childViaVar("seo").child("c"); // key = null
ref.setVar("seo","x"); // key = "a/x/c"
ref.setVar("seo","y"); // key = "a/y/c"
```


