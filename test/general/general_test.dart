import 'dart:convert';

import "package:test/test.dart";

import "package:firebase_powered/general.dart" as DataSource;

main() {
	test("value wrapper", () {
		expect(new DataSource.General(3)[""],3);
		expect(new DataSource.General(3.3)[""],3.3);
		expect(new DataSource.General(null)[""],null);
		expect(new DataSource.General("test")[""],"test");
		expect(new DataSource.General(new DateTime(2010,02,04))[""],new DateTime(2010,02,04));
		expect(new DataSource.General({ 'key': 'value' })[""],{ 'key': 'value' });
	});
	test("type convertors - asString", () {
		expect(new DataSource.General("3").asString(),"3");
		expect(new DataSource.General("3").asString(null, ''),"3");
		expect(new DataSource.General(3).asString(),"3");
		expect(new DataSource.General(3).asString(null, ''),"3");
		expect(new DataSource.General(3.3).asString(),"3.3");
		expect(new DataSource.General(3.3).asString(null, ''),"3.3");
		expect(new DataSource.General(new DateTime(2010,02,04)).asString(),'2010-02-04 00:00:00.000');
		expect(new DataSource.General(new DateTime(2010,02,04)).asString(null, ''),'2010-02-04 00:00:00.000');
		expect(new DataSource.General({'key':'val'}).asString(),JSON.encode({'key':'val'}));
		expect(new DataSource.General({'key':'val'}).asString(null, null),JSON.encode({'key':'val'}));
		expect(new DataSource.General(JSON.encode({'key':'val'})).asString(),JSON.encode({'key':'val'}));
		expect(new DataSource.General(JSON.encode({'key':'val'})).asString(null, null),JSON.encode({'key':'val'}));
	});
	test("type convertors - asInt", () {
		expect(new DataSource.General("43").asInt(),43);
		expect(new DataSource.General("43").asInt(null, 0),43);
		expect(new DataSource.General("43.4").asInt(),43);
		expect(new DataSource.General("43.4").asInt(null, 0),43);
		expect(new DataSource.General(43).asInt(),43);
		expect(new DataSource.General(43).asInt(null, 0),43);
		expect(new DataSource.General(43).asInt() is int,isTrue);
		expect(new DataSource.General(43.3).asInt(),43);
		expect(new DataSource.General(43.3).asInt(null, 0),43);
		expect(new DataSource.General(43.3).asInt() is int,isTrue);
		expect(new DataSource.General("fail").asInt(),0);
		expect(new DataSource.General("fail").asInt(null, 45),45);
		expect(new DataSource.General(new DateTime(2010,02,04)).asInt(),0);
		expect(new DataSource.General(new DateTime(2010,02,04)).asInt(null, 5),5);
	});
	test("type convertors - asDouble", () {
		expect(new DataSource.General("43").asDouble(),43.0);
		expect(new DataSource.General("43").asDouble(null, 0.0),43.0);
		expect(new DataSource.General("43").asDouble() is double,isTrue);
		expect(new DataSource.General("43.4").asDouble(),43.4);
		expect(new DataSource.General("43.4").asDouble(null, 0.0),43.4);
		expect(new DataSource.General("43.4").asDouble() is double,isTrue);
		expect(new DataSource.General(43).asDouble(),43.0);
		expect(new DataSource.General(43).asDouble(null, 0.0),43.0);
		expect(new DataSource.General(43).asDouble() is double,isTrue);
		expect(new DataSource.General(43.3).asDouble(),43.3);
		expect(new DataSource.General(43.3).asDouble(null, 0.0),43.3);
		expect(new DataSource.General(43.3).asDouble() is double,isTrue);
		expect(new DataSource.General("fail").asDouble(),0);
		expect(new DataSource.General("fail").asDouble(null, 45.5),45.5);
		expect(new DataSource.General(new DateTime(2010,02,04)).asDouble(),0.0);
		expect(new DataSource.General(new DateTime(2010,02,04)).asDouble(null, 5.0),5.0);
	});
	test("type convertors - asDataTime", () {
		expect(new DataSource.General('3').asDateTime(),null);
		expect(new DataSource.General('3').asDateTime(null, new DateTime(2010,02,04)),new DateTime(2010,02,04));
		expect(new DataSource.General('3.3').asDateTime(),null);
		expect(new DataSource.General('3.3').asDateTime(null, new DateTime(2010,02,04)),new DateTime(2010,02,04));
		expect(new DataSource.General('fail').asDateTime(),null);
		expect(new DataSource.General('fail').asDateTime(null, new DateTime(2010,02,04)),new DateTime(2010,02,04));
		expect(new DataSource.General(3).asDateTime(),null);
		expect(new DataSource.General(3).asDateTime(null, new DateTime(2010,02,04)),new DateTime(2010,02,04));
		expect(new DataSource.General(3.3).asDateTime(),null);
		expect(new DataSource.General(3.3).asDateTime(null, new DateTime(2010,02,04)),new DateTime(2010,02,04));
		expect(new DataSource.General(new DateTime(2010,02,04)).asDateTime(),new DateTime(2010,02,04));
		expect(new DataSource.General(new DateTime(2010,02,04)).asDateTime(null, new DateTime(2017,02,04)),new DateTime(2010,02,04));
		expect(new DataSource.General("2010-02-04").asDateTime(),new DateTime(2010,02,04));
		expect(new DataSource.General("2010-02-04").asDateTime(null, new DateTime(2017,02,04)),new DateTime(2010,02,04));
	});
	test("type convertors - asMap", () {
		expect(new DataSource.General("43").asMap(),{});
		expect(new DataSource.General("43").asMap(null, {'key':34}),{'key':34});
		expect(new DataSource.General("43").asMap(null, null),null);
		expect(new DataSource.General("43.3").asMap(),{});
		expect(new DataSource.General("43.3").asMap(null, {'key':34}),{'key':34});
		expect(new DataSource.General("43.3").asMap(null, null),null);
		expect(new DataSource.General(43).asMap(),{});
		expect(new DataSource.General(43).asMap(null, {}),{});
		expect(new DataSource.General(43).asMap(null, null),null);
		expect(new DataSource.General(43.3).asMap(),{});
		expect(new DataSource.General(43.3).asMap(null, {}),{});
		expect(new DataSource.General(43.3).asMap(null, null),null);
		expect(new DataSource.General(new DateTime(2010,02,04)).asMap(),{});
		expect(new DataSource.General(new DateTime(2010,02,04)).asMap(null, {'key':'val'}),{'key':'val'});
		expect(new DataSource.General(new DateTime(2010,02,04)).asMap(null, null),null);
		expect(new DataSource.General({'key':'val'}).asMap(),{'key':'val'});
		expect(new DataSource.General({'key':'val'}).asMap(null, null),{'key':'val'});
		expect(new DataSource.General(JSON.encode({'key':'val'})).asMap(),{'key':'val'});
		expect(new DataSource.General(JSON.encode({'key':'val'})).asMap(null, {}),{'key':'val'});
		expect(new DataSource.General("{'key''val'}").asMap(null, null),null);
		expect(new DataSource.General("{'key''val'}").asMap(),{});
		expect(new DataSource.General("{'key''val'}").asMap(null, {'key': 'val'}),{'key': 'val'});
	});
}