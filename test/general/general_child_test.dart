import "package:test/test.dart";

import "package:firebase_powered/general.dart" as DataSource;

import "../testing_data.dart";

main() {
	test("query for child", () {
		expect(new DataSource.General(null)["test"],null);
		DataSource.General v = new DataSource.General(testingData());
		expect(v["dogs/436/info/name"],"Delambree Vittorio");
		expect(v["dogs/436/info/id_breeder"],1);
		expect(v["dogs/436/info/nnx"],null);
		expect(v["dogs/nnx/info/name"],null);
		expect(v["users/2"],{"email":"eva@wapini.cz","main_role":255,"name":"Eva Rokosová"});
	});
	test("typed query for child", () {
		DataSource.General v = new DataSource.General(testingData());
		expect(v.asString("dogs/436/info/name"),"Delambree Vittorio");
		expect(v.asInt("dogs/436/info/id_dam"),138);
		expect(v.asDateTime("dogs/436/info/born"),new DateTime(2007,5,17));
	});
	test("query for missing child", () {
		DataSource.General v = new DataSource.General(testingData());
		expect(v.asString("dogs/nnx/info/name"),"");
		expect(v.asInt("dogs/nnx/info/id_breeder"),0);
		expect(v.asDateTime("dogs/nnx/info/born"),null);
	});
	test("query for missing child with default", () {
		DataSource.General v = new DataSource.General(testingData());
		expect(v.asInt("dogs/nnx/info/id_breeder",30),30);
		expect(v.asDateTime("dogs/nnx/info/born",new DateTime(1985,1,1)),new DateTime(1985,1,1));
	});
	test("set value for child", () {
		DataSource.General v = new DataSource.General(testingData());
		v["dogs/436/info/name"] = "new-name";
		expect(v["dogs/436/info/name"],"new-name");
	});
	test("set value for nonexisting child in empty map", () {
		DataSource.General v = new DataSource.General({});
		v["dogs/436/info/name"] = "new-name";
		expect(v[""],{"dogs":{"436":{"info": {"name":"new-name"}}}});
	});
	test("set child to null", () {
		DataSource.General v = new DataSource.General(testingData());
		v["dogs/436/champions/1958/type"] = null;
		expect(v["dogs/436/champions/1958"],{"country":"CZ"});
	});
	test("set middle node to null", () {
		DataSource.General v = new DataSource.General(testingData());
		v["dogs/436/champions/1958"] = null;
		expect(v["dogs/436/champions"],{"6109":{"country":"CZ","type":"GCh"}});
	});
	test("set all children to null", () {
		DataSource.General v = new DataSource.General(testingData());
		v["dogs/436/champions/1958/type"] = null;
		v["dogs/436/champions/1958/country"] = null;
		expect(v["dogs/436/champions"],{"6109":{"country":"CZ","type":"GCh"}});
	});
	test("set top node (in single path database) to null", () {
		DataSource.General v = new DataSource.General({"a":{"b":{"c":{"d":true}}}});
		v["a"] = null;
		expect(v[""],{});
	});
	test("set middle node (in single path database) to null", () {
		DataSource.General v = new DataSource.General({"a":{"b":{"c":{"d":true}}}});
		v["a/b/c"] = null;
		expect(v[""],{});
	});
	test("set leaf node (in single path database) to null", () {
		DataSource.General v = new DataSource.General({"a":{"b":{"c":{"d":true}}}});
		v["a/b/c/d"] = null;
		expect(v[""],{});
	});
	test("set middle single path node to null", () {
		DataSource.General v = new DataSource.General({"a":{"b":{"c":{"d":true}}},"e":{"f":true}});
		v["a/b/c"] = null;
		expect(v[""],{"e":{"f":true}});
	});
	test("set middle path node to null", () {
		DataSource.General v = new DataSource.General({"a":{"b":{"c":{"d":true}},"g":{"h":true}},"e":{"f":true}});
		v["a/b/c"] = null;
		expect(v[""],{"a":{"g":{"h":true}},"e":{"f":true}});
	});
}