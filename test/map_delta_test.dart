import "package:test/test.dart";

import "package:firebase_powered/map_delta.dart";


main() {
	test("empty stuff", () {
		expect(MapDelta.execute(null, null), {});
		expect(MapDelta.execute({}, null), {});
		expect(MapDelta.execute(null, {}), {});
		expect(MapDelta.execute({}, {}), {});
	});
	test("single level only new data", () {
		expect(MapDelta.execute({"key":"value"}, null), {"key":"value"});
		expect(MapDelta.execute({"key":"value","second":230}, null), {"key":"value","second":230});
	});
	test("multi level only new data", () {
		expect(MapDelta.execute({"key":"value","nested": {"first":"1","second":"2"}}, null), {"key":"value","nested/first":"1","nested/second":"2"});
		expect(MapDelta.execute({"key":"value","nested": {"first":"1","second":{"third":"3"}}}, null), {"key":"value","nested/first":"1","nested/second/third":"3"});
	});
	test("single level only base data", () {
		expect(MapDelta.execute(null,{"key":"value"}), {"key":null});
		expect(MapDelta.execute(null,{"key":"value","second":230}), {"key":null,"second":null});
	});
	test("multi level only base data", () {
		expect(MapDelta.execute(null,{"key":"value","nested": {"first":"1","second":"2"}}), {"key":null,"nested/first":null,"nested/second":null});
		expect(MapDelta.execute(null,{"key":"value","nested": {"first":"1","second":{"third":"3"}}}), {"key":null,"nested/first":null,"nested/second/third":null});
	});
	test("single level difference", () {
		expect(MapDelta.execute({"key":"value"}, {"key":"orig_value"}),{"key":"value"});
	});
	test("single level same data", () {
		expect(MapDelta.execute({"key":"value"}, {"key":"value"}),{});
	});
	test("single level difference/same/new/base data", () {
		expect(MapDelta.execute({"key":"value","second":"sec","third":"thi"}, {"second":"sec","third":"third","fourth":"fou"}),{"key":"value","third":"thi","fourth":null});
	});
	test("multi level complex",() {
		expect(MapDelta.execute(
			{
				"name":"Děčín",
				"country": {
					"name": "Czech Republic",
					"abbr": "CZ"
				},
				"member": {
					"nato":"North Atlantic Treaty Organization",
					"eu":"European Union"
				},
				"capital": "Prague"
			},
			{
				"name":"Děčín",
				"country": {
					"name": "Czech Republic"
				},
				"member": {
					"vs":"Varšavská smlouva"
				},
				"capital": "Praha"
			}
		),
			{
				"country/abbr": "CZ",
				"member/nato": "North Atlantic Treaty Organization",
				"member/eu": "European Union",
				"member/vs": null,
				"capital": "Prague"
			}
		);
	});
}

