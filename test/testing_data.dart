import "dart:convert";

Map testingData() {
	return JSON.decode(JSON.encode(dataStub));
}

Map dataStub = {
	"dogs": {
		"436": {
			"champions" : {
				"1958" : {
					"country" : "CZ",
					"type" : "Ch"
				},
				"6109" : {
					"country" : "CZ",
					"type" : "GCh"
				}
			},
			"exams" : {
				"1" : true,
				"2" : true,
				"3" : true,
				"4" : true,
				"7" : true,
				"8" : true,
				"9" : true,
				"10" : true,
				"15" : true,
				"17" : true,
				"24" : true,
				"25" : true,
				"89" : true,
				"90" : true
			},
			"health" : {
				"1" : true,
				"26" : true,
				"51" : true,
				"55" : true,
				"57" : true,
				"59" : true,
				"64" : true
			},
			"info" : {
				"autocomplete_name" : "Delambree Vittorio Wapini von Folge - ČMKU/HHK/172/07",
				"born" : "2007-05-17",
				"color" : "zlatě žíhaná",
				"dna_profile" : "1",
				"height" : "61",
				"home_name" : "Dan",
				"id_admin" : "2",
				"id_breed" : "4",
				"id_breeder" : 1,
				"id_dam" : "138",
				"id_owner" : "6878",
				"id_sire" : "68",
				"male" : "1",
				"name" : "Delambree Vittorio",
				"registration_no" : "ČMKU/HHK/172/07",
				"seo_name" : "delambree-vittorio-wapini-von-folge-cmku-hhk-172-07",
				"stud" : "1",
				"stud_code" : "B1N0",
				"weight" : "33"
			}
		},
		"19": {
			"info": {
				"autocomplete_name" : "Francesca Fortés Wapini von Folge - CMKU/HHK/250/09",
				"born" : "2009-05-29",
				"color" : "zlatě žíhaná",
				"dna_profile" : "1",
				"height" : "57",
				"home_name" : "Freny",
				"id_admin" : "2",
				"id_breed" : "4",
				"id_breeder" : "1",
				"id_dam" : "2",
				"id_owner" : "1",
				"id_sire" : "1",
				"interchampion" : "2",
				"name" : "Francesca Fortés",
				"registration_no" : "CMKU/HHK/250/09",
				"seo_name" : "francesca-fortes-wapini-von-folge-cmku-hhk-250-09",
				"stud" : "1",
				"stud_code" : "J1N0",
				"weight" : "26"
			}
		},
		"1": {
			"info": {
				"autocomplete_name" : "Nitro v.d. Vastenow - NHSB 2437523",
				"born" : "2002-09-18",
				"color" : "zlatě žíhaná",
				"home_name" : "Nitro",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "12",
				"id_sire" : "11",
				"male" : "1",
				"name" : "Nitro",
				"registration_no_original" : "NHSB 2437523",
				"seo_name" : "nitro-v-d-vastenow-nhsb-2437523",
				"stud" : "1"
			}
		},
		"2": {
			"info": {
				"autocomplete_name" : "Cézanne Lorrain Wapini von Folge - ČMKU/HHK/84/04/06",
				"born" : "2004-02-05",
				"color" : "zlatě žíhaná",
				"dna_profile" : "1",
				"height" : "57",
				"home_name" : "Orina",
				"id_admin" : "2",
				"id_breed" : "4",
				"id_breeder" : "1",
				"id_dam" : "5",
				"id_owner" : "1",
				"id_sire" : "4",
				"interchampion" : "2",
				"name" : "Cézanne Lorrain",
				"registration_no" : "ČMKU/HHK/84/04/06",
				"seo_name" : "cezanne-lorrain-wapini-von-folge-cmku-hhk-84-04-06",
				"stud" : "1",
				"weight" : "28"
			}
		},
		"68": {
			"info": {
				"autocomplete_name" : "Havrevingens Manic - NHSB 2375049",
				"born" : "2002-01-01",
				"id_breed" : "4",
				"id_breeder" : "16",
				"id_dam" : "75",
				"id_sire" : "72",
				"male" : "1",
				"name" : "Manic",
				"registration_no" : "S14446/2002",
				"registration_no_original" : "NHSB 2375049",
				"seo_name" : "havrevingens-manic-nhsb-2375049",
				"stud" : "1"
			}
		},
		"138": {
			"info": {
				"autocomplete_name" : "Balla D´France Wapini von Folge - ČMKU/HHK/74/03/06",
				"born" : "2003-10-28",
				"home_name" : "Freny",
				"id_breed" : "4",
				"id_breeder" : "1",
				"id_dam" : "25",
				"id_owner" : "104",
				"id_sire" : "132",
				"interchampion" : "2",
				"name" : "Balla D´France",
				"registration_no" : "ČMKU/HHK/74/03/06",
				"seo_name" : "balla-d-france-wapini-von-folge-cmku-hhk-74-03-06",
				"stud" : "1"
			}
		},
		"75": {
			"info":{
				"autocomplete_name" : "Rha v.d. Vastenow - S24559/97",
				"born" : "1996-08-05",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "83",
				"id_owner" : "42",
				"id_sire" : "82",
				"name" : "Rha",
				"registration_no" : "NHSB 2084560",
				"registration_no_original" : "S24559/97",
				"seo_name" : "rha-v-d-vastenow-s24559-97",
				"stud" : "1"
			}
		},
		"72": {
			"info": {
				"autocomplete_name" : "Havrevingens Furie - S54319/97",
				"born" : "1997-10-15",
				"id_breed" : "4",
				"id_breeder" : "16",
				"id_dam" : "77",
				"id_sire" : "60558",
				"male" : "1",
				"name" : "Furie",
				"registration_no" : "NHSB 2275386",
				"registration_no_original" : "S54319/97",
				"seo_name" : "havrevingens-furie-s54319-97",
				"stud" : "1"
			}
		},
		"77": {
			"info": {
				"autocomplete_name" : "Anca ´t Hollanderke - NHSB 1807891",
				"born" : "1992-02-16",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "445",
				"id_dam" : "131",
				"id_sire" : "130",
				"name" : "Anca",
				"registration_no_original" : "NHSB 1807891",
				"seo_name" : "anca-t-hollanderke-nhsb-1807891",
				"stud" : "1"
			}
		},
		"60558": {
			"info": {
				"autocomplete_name" : "Iamos du Bourg Bernard - LOF 142/31",
				"born" : "1993-04-17",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "397",
				"id_dam" : "129",
				"id_sire" : "78",
				"interchampion" : "2",
				"male" : "1",
				"name" : "Iamos",
				"registration_no_original" : "LOF 142/31",
				"seo_name" : "iamos-du-bourg-bernard-lof-142-31",
				"stud" : "1"
			}
		},
		"129": {
			"info":{
				"autocomplete_name" : "Fly de la Clairiere des Fantomes Gris - LOF 40/21",
				"born" : "1990-04-05",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "155",
				"id_dam" : "156",
				"id_sire" : "155",
				"name" : "Fly",
				"registration_no_original" : "LOF 40/21",
				"seo_name" : "fly-de-la-clairiere-des-fantomes-gris-lof-40-21",
				"stud" : "1"
			}
		},
		"78": {
			"info": {
				"autocomplete_name" : "Guykko du Bois Schelblot - LOF 75/17",
				"id_breed" : "4",
				"id_breeder" : "411",
				"id_dam" : "154",
				"id_sire" : "153",
				"male" : "1",
				"name" : "Guykko",
				"registration_no_original" : "LOF 75/17",
				"seo_name" : "guykko-du-bois-schelblot-lof-75-17",
				"stud" : "1"
			}
		},
		"82": {
			"info": {
				"autocomplete_name" : "Menno-Twee v.d. Vastenow - NHSB 1909109",
				"born" : "1993-10-30",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "113",
				"id_sire" : "124",
				"male" : "1",
				"name" : "Menno-Twee",
				"registration_no_original" : "NHSB 1909109",
				"seo_name" : "menno-twee-v-d-vastenow-nhsb-1909109",
				"stud" : "1"
			}
		},
		"83": {
			"info": {
				"autocomplete_name" : "Hertha v.d. Vastenow - NHSB 1726464",
				"born" : "1990-09-15",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "115",
				"id_sire" : "116",
				"name" : "Hertha",
				"registration_no_original" : "NHSB 1726464",
				"seo_name" : "hertha-v-d-vastenow-nhsb-1726464",
				"stud" : "1"
			}
		},
		"25": {
			"info": {
				"autocomplete_name" : "Akela Qalt Mercator - ČMKU/HHK/4/97/99",
				"born" : "1997-06-09",
				"color" : "zlatě žíhaná",
				"death" : "2008-12-14",
				"height" : "57",
				"home_name" : "Kely",
				"id_breed" : "4",
				"id_breeder" : "3",
				"id_dam" : "10",
				"id_owner" : "1",
				"id_sire" : "9",
				"interchampion" : "2",
				"name" : "Akela",
				"registration_no" : "ČMKU/HHK/4/97/99",
				"seo_name" : "akela-qalt-mercator-cmku-hhk-4-97-99",
				"stud" : "1"
			}
		},
		"132": {
			"info": {
				"autocomplete_name" : "Dries v.d. Vastenow - NHSB 2313780",
				"born" : "2000-07-16",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "137",
				"id_sire" : "125",
				"male" : "1",
				"name" : "Dries",
				"registration_no_original" : "NHSB 2313780",
				"seo_name" : "dries-v-d-vastenow-nhsb-2313780",
				"stud" : "1"
			}
		},
		"137" : {
			"info":{
				"autocomplete_name" : "Reza v.d. Vastenow - NHSB 2084563",
				"born" : "1996-08-05",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "83",
				"id_owner" : "4",
				"id_sire" : "82",
				"name" : "Reza",
				"registration_no_original" : "NHSB 2084563",
				"seo_name" : "reza-v-d-vastenow-nhsb-2084563",
				"stud" : "1"
			}
		},
		"125": {
			"info":{
				"autocomplete_name" : "Aron-Thijs v.d. Strooming - NHSB 1695771",
				"born" : "1990-02-25",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "164",
				"id_dam" : "128",
				"id_sire" : "127",
				"male" : "1",
				"name" : "Aron-Thijs",
				"registration_no" : "NHSB 1695771",
				"registration_no_original" : "NHSB 1695771",
				"seo_name" : "aron-thijs-v-d-strooming-nhsb-1695771",
				"stud" : "1"
			}
		},
		"10": {
			"info": {
				"autocomplete_name" : "Gabie v.d. Passchin - NHSB 1965301",
				"born" : "1994-10-03",
				"death" : "2008-04-05",
				"id_breed" : "4",
				"id_breeder" : "20",
				"id_dam" : "98",
				"id_owner" : "3",
				"id_sire" : "97",
				"interchampion" : "2",
				"name" : "Gabie",
				"registration_no" : "ČMKU/HHK/1/-95/94/96",
				"registration_no_original" : "NHSB 1965301",
				"seo_name" : "gabie-v-d-passchin-nhsb-1965301",
				"stud" : "1"
			}
		},
		"9": {
			"info": {
				"autocomplete_name" : "Josco v.d. Vastenow - NHSB 1812968",
				"born" : "1992-03-06",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "113",
				"id_sire" : "112",
				"male" : "1",
				"name" : "Josco",
				"registration_no" : "NHSB 1812968",
				"registration_no_original" : "NHSB 1812968",
				"seo_name" : "josco-v-d-vastenow-nhsb-1812968",
				"stud" : "1"
			}
		},
		"12": {
			"info":{
				"autocomplete_name" : "Fenny - NHSB 2316644",
				"born" : "2000-09-25",
				"id_breed" : "4",
				"id_breeder" : "489",
				"id_dam" : "149",
				"id_sire" : "125",
				"name" : "Fenny",
				"registration_no_original" : "NHSB 2316644",
				"seo_name" : "fenny-nhsb-2316644",
				"stud" : "1"
			}
		},
		"11": {
			"info": {
				"autocomplete_name" : "Oscar v.d. Vastenow - S52917/97",
				"born" : "1994-07-04",
				"death" : "1900-01-01",
				"height" : "63",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "83",
				"id_sire" : "116",
				"male" : "1",
				"name" : "Oscar",
				"registration_no" : "NHSB 1948184",
				"registration_no_original" : "S52917/97",
				"seo_name" : "oscar-v-d-vastenow-s52917-97",
				"stud" : "1",
				"weight" : "30"
			}
		},
		"116": {
			"info": {
				"autocomplete_name" : "Toby v.d. Vastenow - NHSB 1502314",
				"born" : "1986-10-07",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "121",
				"id_sire" : "123",
				"male" : "1",
				"name" : "Toby",
				"registration_no_original" : "NHSB 1502314",
				"seo_name" : "toby-v-d-vastenow-nhsb-1502314",
				"stud" : "1"
			}
		},
		"149": {
			"info": {
				"autocomplete_name" : "Linda - NHSB 2224059",
				"born" : "1998-10-02",
				"id_breed" : "4",
				"id_breeder" : "485",
				"id_dam" : "81",
				"id_sire" : "80",
				"name" : "Linda",
				"registration_no_original" : "NHSB 2224059",
				"seo_name" : "linda-nhsb-2224059",
				"stud" : "1"
			}
		},
		"5" : {
			"info": {
				"autocomplete_name" : "Caddy Qalt Mercator - ČMKU/HHK/19/99/01",
				"born" : "1999-04-03",
				"color" : "zlatě žíhaná",
				"death" : "2004-04-13",
				"height" : "56",
				"home_name" : "Kačenka",
				"id_breed" : "4",
				"id_breeder" : "3",
				"id_dam" : "10",
				"id_owner" : "1",
				"id_sire" : "9",
				"interchampion" : "2",
				"name" : "Caddy",
				"registration_no" : "ČMKU/HHK/19/99/01",
				"seo_name" : "caddy-qalt-mercator-cmku-hhk-19-99-01",
				"stud" : "1",
				"weight" : "24"
			}
		},
		"4": {
			"info": {
				"autocomplete_name" : "Aron v.d. Vastenow - NHSB 2222103",
				"born" : "1999-01-05",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "126",
				"id_sire" : "125",
				"male" : "1",
				"name" : "Aron",
				"registration_no_original" : "NHSB 2222103",
				"seo_name" : "aron-v-d-vastenow-nhsb-2222103",
				"stud" : "1"
			}
		},
		"126": {
			"info": {
				"autocomplete_name" : "Olga v.d. Vastenow - NHSB 1948188",
				"born" : "1994-07-04",
				"death" : "1900-01-01",
				"id_breed" : "4",
				"id_breeder" : "4",
				"id_dam" : "83",
				"id_sire" : "116",
				"name" : "Olga",
				"registration_no_original" : "NHSB 1948188",
				"seo_name" : "olga-v-d-vastenow-nhsb-1948188",
				"stud" : "1"
			}
		}
	},
	"breeders": {
		"1": {
			"append_type" : "append",
			"autocomplete_name" : "Wapini von Folge / Eva Rokosová (Děčín 2 )",
			"id_owner" : "1",
			"name" : "Wapini von Folge",
			"seo_name" : "wapini-von-folge-eva-rokosova-decin-2"
		}
	},
	"matings": {
		"632": {
			"info" : {
				"id_breed" : "4",
				"id_breeder" : "1",
				"id_dam" : "78041",
				"id_sire" : "96625",
				"plan_date" : "2000-01-01",
				"status" : "0"
			}
		}
	},
	"humans": {
		"1" : {
			"autocomplete_name" : "Eva Rokosová (Děčín 2 )",
			"city" : "Děčín 2",
			"email" : "eva@wapini.cz",
			"first_name" : "Eva",
			"id_admin" : "2",
			"id_country" : "CZ",
			"last_name" : "Rokosová",
			"phone" : "+420 777012702",
			"seo_name" : "eva-rokosova-decin-2",
			"street" : "Riegrova 10",
			"web" : "www.wapini.cz",
			"zip" : "40502"
		}
	},
	"users": {
		"2": {
			"email" : "eva@wapini.cz",
			"main_role" : 255,
			"name" : "Eva Rokosová"
		}
	}
};