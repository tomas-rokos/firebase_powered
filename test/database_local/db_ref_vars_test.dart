import "package:test/test.dart";
import "../testing_data.dart";

import "package:firebase_powered/firebase_powered.dart" as fp;

void main() {
	fp.database().setLocalData(null, testingData());
	group("DbRef.vars", () {

		test('get without set', () async {
			fp.DbRef p = fp.database().ref("breeds/1/name");
			expect(p.getVar("key"), null);
		});
		test('simple set', () async {
			fp.DbRef p = fp.database().ref("breeds/1");
			p.setVar("key", "on");
			expect(p.getVar("key"), "on");
		});
		test('re-set', () async {
			fp.DbRef p = fp.database().ref("breeds/1");
			p.setVar("key", "on");
			p.setVar("key", "off");
			expect(p.getVar("key"), "off");
		});
		test('key without set var', () async {
			fp.DbRef p = fp.database().ref("breeds").childViaVar("key").child("info");
			expect(p.key, null);
		});
		test('key with var', () async {
			fp.DbRef p = fp.database().ref("breeds").childViaVar("key").child("info");
			p.setVar("key", "1");
			expect(p.key, "breeds/1/info");
		});
		test('key with var re-set', () async {
			fp.DbRef p = fp.database().ref("breeds").childViaVar("key").child("info");
			p.setVar("key", "1");
			expect(p.key, "breeds/1/info");
			p.setVar("key", "2");
			expect(p.key, "breeds/2/info");
		});
		test('key with var nulled', () async {
			fp.DbRef p = fp.database().ref("breeds").childViaVar("key").child("info");
			p.setVar("key", "1");
			expect(p.key, "breeds/1/info");
			p.setVar("key", null);
			expect(p.key, null);
		});
	});
}

