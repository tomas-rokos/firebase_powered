import "package:test/test.dart";
import "../testing_data.dart";

import "package:firebase_powered/firebase_powered.dart" as fp;

void main() {
	fp.database().setLocalData(null, testingData());
	group("DbRef.value", () {

		test('direct value', () async {
		 	fp.DbRef p = fp.database().ref("dogs/436/info/home_name");
			expect(p.value, "Dan");
		});
		test('direct map', () async {
			fp.DbRef p = fp.database().ref("dogs/436/info");
			expect(p.value["home_name"], "Dan");
		});
	});
}

