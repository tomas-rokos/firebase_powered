import "dart:async";
import "dart:convert";
import "package:test/test.dart";
import "../testing_data.dart";

import "package:firebase_powered/firebase_powered.dart" as fp;

void main() {
	group("DbRef.onValue", () {
		setUp(() {
			fp.database().setLocalData(null, testingData());
		});

		test('direct value', () async {
			fp.DbRef p = fp.database().ref("dogs/436/info/home_name");
			expect(p.onValue,emits("Dan"));
		});
		test('missing value', () async {
			fp.DbRef p = fp.database().ref("dogs/436/info/work_name");
			expect(p.onValue,emits(null));
		});
		test('two listeners', () async {
			fp.DbRef p = fp.database().ref("dogs/436/info/home_name");
			List<String> result = [];
			p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			p.onValue.listen((dynamic d) {
				result.add("b:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:\"Dan\"","b:\"Dan\""]);
		});
		test('two listeners with update', () async {
			fp.DbRef p = fp.database().ref("dogs/436/info/home_name");
			List<String> result = [];
			p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			p.onValue.listen((dynamic d) {
				result.add("b:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			fp.database().update({"dogs/436/info/home_name":"Tonda"});
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:\"Dan\"","b:\"Dan\"","a:\"Tonda\"","b:\"Tonda\""]);
		});
		test('two listeners with update and disconnect', () async {
			fp.DbRef p = fp.database().ref("dogs/436/info/home_name");
			List<String> result = [];
			p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			p.onValue.listen((dynamic d) {
				result.add("b:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			p.onValueCancelAll();
			await new Future.delayed(new Duration(milliseconds: 1));
			fp.database().update({"dogs/436/info/home_name":"Tonda"});
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:\"Dan\"","b:\"Dan\""]);
		});
		test('two listeners with update and cancel', () async {
			fp.DbRef p = fp.database().ref("dogs/436/info/home_name");
			List<String> result = [];
			StreamSubscription s = p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			p.onValue.listen((dynamic d) {
				result.add("b:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			s.cancel();
			await new Future.delayed(new Duration(milliseconds: 1));
			fp.database().update({"dogs/436/info/home_name":"Tonda"});
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:\"Dan\"","b:\"Dan\"","b:\"Tonda\""]);
		});
		test('two listeners with update and cancel', () async {
			fp.DbRef p = fp.database().ref("dogs/436/info/home_name");
			List<String> result = [];
			StreamSubscription s = p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			StreamSubscription s2 = p.onValue.listen((dynamic d) {
				result.add("b:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			s.cancel();
			s2.cancel();
			await new Future.delayed(new Duration(milliseconds: 1));
			fp.database().update({"dogs/436/info/home_name":"Tonda"});
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:\"Dan\"","b:\"Dan\""]);
		});
		test('listener with var', () async {
			fp.DbRef p = fp.database().ref("dogs").childViaVar("dogid").child("info/home_name");
			List<String> result = [];
			p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:null"]);
		});
		test('listener with changed var', () async {
			fp.DbRef p = fp.database().ref("dogs").childViaVar("dogid").child("info/home_name");
			List<String> result = [];
			p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			p.setVar("dogid", "436");
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:null","a:\"Dan\""]);
		});
		test('listener with changed and nulled var', () async {
			fp.DbRef p = fp.database().ref("dogs").childViaVar("dogid").child("info/home_name");
			List<String> result = [];
			p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			p.setVar("dogid", "436");
			await new Future.delayed(new Duration(milliseconds: 1));
			p.setVar("dogid", null);
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:null","a:\"Dan\"","a:null"]);
		});
		test('listener and update nested', () async {
			fp.DbRef p = fp.database().ref("dogs/436/champions/1958");
			List<String> result = [];
			p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			fp.database().update({"dogs/436/champions/1958/type":"GCh"});
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:{\"country\":\"CZ\",\"type\":\"Ch\"}","a:{\"country\":\"CZ\",\"type\":\"GCh\"}"]);
		});
		test('listener and update upper node', () async {
			fp.DbRef p = fp.database().ref("dogs/436/champions/1958");
			List<String> result = [];
			p.onValue.listen((dynamic d) {
				result.add("a:"+JSON.encode(d));
			});
			await new Future.delayed(new Duration(milliseconds: 1));
			fp.database().update({"dogs/436/champions":"GCh"});
			await new Future.delayed(new Duration(milliseconds: 1));
			expect(result,["a:{\"country\":\"CZ\",\"type\":\"Ch\"}","a:null"]);
		});
	});
}

