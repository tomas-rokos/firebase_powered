import "package:test/test.dart";

import "package:firebase_powered/firebase_powered.dart" as fp;

void main() {
	group("database.update", () {
		setUp(() {
		});

		test('one value', () async {
			fp.database().update({"one":"two"});
			fp.DbRef p = fp.database().ref("one");
			expect(p.value, "two");
		});
		test('multiple values', () async {
			fp.database().update({"test/name":"nm","test/born":"br"});
			fp.DbRef p = fp.database().ref("test");
			expect(p.value, {"name":"nm","born":"br"});
		});
		test('multiple values, different level', () async {
			fp.database().update({"test/name/first":"fn","test/name/last":"ls","test/born":"br"});
			fp.DbRef p = fp.database().ref("test");
			expect(p.value, {"name":{"first":"fn","last":"ls"},"born":"br"});
		});
		test('removal by null', () async {
			fp.database().update({"test/name/first":"fn","test/name/last":"ls","test/born":"br"});
			fp.database().update({"test/born":null});
			fp.DbRef p = fp.database().ref("test");
			expect(p.value, {"name":{"first":"fn","last":"ls"}});
		});
		test('removal by null - parent node', () async {
			fp.database().update({"test/name/first":"fn","test/name/last":"ls","test/born":"br"});
			fp.database().update({"test/name":null});
			fp.DbRef p = fp.database().ref("test");
			expect(p.value, {"born":"br"});
		});
		test("getting newly created data to existing DbRef", () async {
			fp.DbRef p = fp.database().ref("dogs/xx");
			expect(p.value,null);
			fp.database().update({"dogs/xx":true});
			expect(p.value,true);
		});
		test("getting newly created nested data to existing DbRef", () async {
			fp.DbRef p = fp.database().ref("test/xx");
			expect(p.value,null);
			fp.database().update({"test/xx/yy":true});
			expect(p.value,{"yy":true});
		});
 	});
}

