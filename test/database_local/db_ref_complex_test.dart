import "package:test/test.dart";
import "../testing_data.dart";

import "package:firebase_powered/firebase_powered.dart" as fp;

void main() {
//	fp.database().setLocalData(null, testingData());
//	group("DbRef.complexValue", () {
//
//		test('marking DbRef with name via as', () async {
//			fp.DbRef p = fp.database().ref("dogs/436/info/home_name").as("hname");
//			await p.onComplexValue.first;
//			Map<String,dynamic> cpl = p.complexValue;
//			expect(cpl.length, 1);
//			expect(cpl.keys, ["hname"]);
//			expect(cpl.values, ["Dan"]);
//		});
//		test('two complex values', () async {
//			fp.DbRef p = fp.database().ref("dogs/436/info/home_name").as("sire").root.child("dogs/19/info/home_name").as("dam");
//			await p.onComplexValue.first;
//			Map<String,dynamic> cpl = p.complexValue;
//			expect(cpl.length, 2);
//			expect(cpl.keys, ["sire","dam"]);
//			expect(cpl.values, ["Dan","Freny"]);
//			expect(cpl["sire"], "Dan");
//			expect(cpl["dam"], "Freny");
//		});
//		test('childViaVar from DbRef', () async {
//			fp.DbRef p = fp.database().ref("dogs/436/info").as("dog").root.child("dogs").childViaVar("dog","id_sire").child("info").as("sire");
//			await p.onComplexValue.first;
//			Map<String,dynamic> cpl = p.complexValue;
//			expect(cpl.length, 1);
//			expect(cpl.keys, ["dog"]);
//			expect(cpl["dog"]["name"], "Delambree Vittorio");
//			await p.onComplexValue.first;
//			expect(cpl.length, 2);
//			expect(cpl.keys, ["dog","sire"]);
//			expect(cpl["dog"]["name"], "Delambree Vittorio");
//			expect(cpl["sire"]["name"], "Manic");
//		});
//	});
}

