import "package:test/test.dart";

import "package:firebase_powered/firebase_powered.dart" as fp;

void main() {
	group("DbRef.key", () {
		test('just ref', () async {
			fp.DbRef p = fp.database().ref("breeds/1");
			expect(p.key, "breeds/1");
		});
		test('ref + child', () async {
			fp.DbRef p = fp.database().ref("breeds/1").child("name");
			expect(p.key, "breeds/1/name");
		});
		test('just child', () async {
			fp.DbRef p = fp.database().ref().child("breeds");
			expect(p.key, "breeds");
		});
		test('multiple child', () async {
			fp.DbRef p = fp.database().ref().child("breeds").child("1").child("name");
			expect(p.key, "breeds/1/name");
		});
		test("root at beggining", () async {
			fp.DbRef p = fp.database().ref().root.child("breeds").child("1").child("name");
			expect(p.key, "breeds/1/name");
		});
		test("root in the middle", () async {
			fp.DbRef p = fp.database().ref().child("breeds").child("1").root.child("name");
			expect(p.key, "name");
		});
		test("root at the end", () async {
			fp.DbRef p = fp.database().ref().child("breeds").child("1").child("name").root;
			expect(p.key, null);
		});
		test("parent on empty", () async {
			fp.DbRef p = fp.database().ref().parent;
			expect(p.key, null);
		});
		test("parent on single", () async {
			fp.DbRef p = fp.database().ref().child("breeds").parent;
			expect(p.key, null);
		});
		test("parent on two items", () async {
			fp.DbRef p = fp.database().ref().child("breeds").child("1").parent;
			expect(p.key, "breeds");
		});
		test("parent on path", () async {
			fp.DbRef p = fp.database().ref().child("breeds").child("1").child("name").parent;
			expect(p.key, "breeds/1");
		});
		test("double parent on path", () async {
			fp.DbRef p = fp.database().ref().child("breeds").child("1").child("name").parent.parent;
			expect(p.key, "breeds");
		});
		test("parent and child", () async {
			fp.DbRef p = fp.database().ref().child("breeds").child("1").child("name").parent.child("born");
			expect(p.key, "breeds/1/born");
		});
	});
}

