import "package:test/test.dart";

import "package:firebase_powered/firebase_powered.dart" as fp;

void main() {
	group("database.update splayed", () {
		setUp(() {
		});

		test('multiple values', () async {
			fp.database().update({"test/name":"nm","test/born":"br"});
			fp.DbRef p = fp.database().ref("test");
			expect(p.value.keys,orderedEquals(["born","name"]));
		});
		test('multiple values, different level', () async {
			fp.database().update({"test/name/first":"fn","test/name/last":"ls","test/born":"br"});
			fp.DbRef p = fp.database().ref("test");
			expect(p.value.keys,orderedEquals(["born","name"]));
			expect(p.value["name"].keys,orderedEquals(["first","last"]));
		});
	});
}

