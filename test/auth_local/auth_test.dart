import "package:test/test.dart";

import "package:firebase_powered/firebase_powered.dart" as fp;

void main() {
	group("Auth", () {

		test('signin', () async {
			await fp.auth().signInWithRedirect(new fp.GoogleAuthProvider());
		});
		test('signout', () async {
			await fp.auth().signOut();
		});
		test('signin/signout', () async {
			await fp.auth().signInWithRedirect(new fp.GoogleAuthProvider());
			await fp.auth().signOut();
		});
		test('currentUser', () async {
			expect(fp.auth().currentUser,isNull);
		});
	});
}

