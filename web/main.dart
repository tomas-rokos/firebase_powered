import "dart:html";
import "dart:convert";

import 'package:firebase_powered/firebase_powered.dart' as firebase;
import 'package:firebase_powered/firebase.dart' as firebase;

void main() {
	firebase.initializeApp(
		apiKey: "AIzaSyApMsSCCzy81YB1OgBUUZLbTxT6EBPgCDY",
		authDomain: "playground-a8df2.firebaseapp.com",
		databaseURL: "https://playground-a8df2.firebaseio.com",
//		projectId: "playground-a8df2",
		storageBucket: "",
//		messagingSenderId: "948022233563"
	);

	register("Changes",rqChanges);
	register("Full Database",rqFullDatabase);
	register("Simple Query",rqSimpleQuery);
	register("Simple Query Nested path",rqSimpleQueryNestedPath);
	register("Waiting for connect",rqWaitForConnect);
	register("Direct value call",rqDirectValueCall);
	register("Variable Query",rqVariableQuery);
	register("Named Refs",rqNamedRefs);
	register("Nested Refs",rqNestedRefs);
	register("Nested Variable Refs",rqNestedVariableRefs);
}
int lastMilliseconds = null;

void addToLog(Object o,String s, [Map cmds]) {
	DivElement rec = new DivElement();
	document.querySelector("#logger").append(rec);
	DivElement ts = new DivElement();
	ts.classes.add("timestamp");
	DateTime tm = new DateTime.now();
	if (lastMilliseconds == null) {
		lastMilliseconds = tm.millisecondsSinceEpoch;
	}
	ts.text = (tm.millisecondsSinceEpoch-lastMilliseconds).toString();
	rec.append(ts);
	rec.append(new DivElement()..classes.add("hash")..text=o.hashCode.toString());
	rec.append(new PreElement()..text=s);

	if (cmds != null) {
		DivElement cmdsdiv = new DivElement();
		cmdsdiv.classes.add("commands");
		rec.append(cmdsdiv);
		cmds.forEach((String cmd, cmdfn) {
			DivElement cmddiv = new DivElement();
			cmdsdiv.append(cmddiv);
			cmddiv.text = cmd;
			cmddiv.onClick.listen((_) {
				addToLog(o,cmd);
				if (cmdfn() != null)
					cmdsdiv.remove();
			});
		});

	}
	rec.scrollIntoView();
}

void register(String name,handlerfn) {
	Element selector = document.querySelector("#selector");
	DivElement clicker = new DivElement();
	selector.append(clicker);
	clicker.text = name;
	clicker.onClick.listen(handlerfn);
}
void rqFullDatabase(x) {
	firebase.DbRef ref = firebase.database().ref("/");
	addToLog(ref,"database().ref(\"/\")",{
		"DbRef.onValueCancelAll": () {
			ref.onValueCancelAll();
			return true;
		}
	});
	ref.onValue.listen((dynamic v) {
		JsonEncoder encoder = new JsonEncoder.withIndent('  ');
		addToLog(ref,"onValue: "+encoder.convert(v));
	});
}

void rqChanges(x) {
	addToLog(firebase.database(),"database().update(\"/\")",{
		"cities/1/name/Děčín": () {
			firebase.database().update({"cities/1/name":"Děčín"});
		},
		"cities/1/name/Děčín2": () {
			firebase.database().update({"cities/1/name":"Děčín2"});
		}
	});
}

void rqWaitForConnect(x) {
	firebase.DbRef ref = firebase.database().ref("cities/1/name");
	ref.waitForRemoteValue().then((val) {
		addToLog(ref,"database().ref(\"cities/1/name\").waitForRemoteValue().then => value: "+val.toString());
	});
}

void rqDirectValueCall(x) {
	firebase.DbRef ref = firebase.database().ref("cities/1/name");
	addToLog(ref,"database().ref(\"cities/1/name\").value: "+ref.value.toString());
}

void rqSimpleQuery(x) {
	firebase.DbRef ref = firebase.database().ref("cities/1");
	addToLog(ref,"database().ref(\"cities/1\")",{
		"DbRef.onValueCancelAll": () {
			ref.onValueCancelAll();
			return true;
		}
	});
	ref.onValue.listen((dynamic v) {
		addToLog(ref,"onValue: "+JSON.encode(v));
	});
}

void rqSimpleQueryNestedPath(x) {
	firebase.DbRef ref = firebase.database().ref("cities/1/name");
	addToLog(ref,"database().ref(\"cities/1/name\")",{
		"DbRef.onValueCancelAll": () {
			ref.onValueCancelAll();
			return true;
		}
	});
	ref.onValue.listen((dynamic v) {
		addToLog(ref,"onValue: "+JSON.encode(v));
	});
}

void rqVariableQuery(x) {
	firebase.DbRef ref = firebase.database().ref("cities").childViaVar("city");
	addToLog(ref,"database().ref(\"cities\").childViaVar(\"city\")",{
		"DbRef.onValueCancelAll": () {
			ref.onValueCancelAll();
			return true;
		},
		"DbRef.setVar(\"city\",\"1\")": () {
			ref.setVar("city","1");
		},
		"DbRef.setVar(\"city\",\"2\")": () {
			ref.setVar("city","2");
		},
		"DbRef.setVar(\"city\",null)": () {
			ref.setVar("city",null);
		}
	});
	ref.onValue.listen((dynamic v) {
		addToLog(ref,"onValue: "+JSON.encode(v));
	});
}

void rqNamedRefs(x) {
	firebase.DbRef ref = firebase.database().ref().child("cities/1/name").as("city_1").
							root.child("cities/2/name").as("city_2");
	addToLog(ref,"database().ref().child(\"cities/1/name\").as(\"city_1\").\nroot.child(\"cities/2/name\").as(\"city_2\")",{
		"DbRef.onValueCancelAll": () {
			ref.onValueCancelAll();
			return true;
		}
	});
//	ref.onComplexValue.listen((dynamic v) {
//		addToLog(ref,"onComplexValue: "+JSON.encode(v));
//	});

}

void rqNestedRefs(x) {
	firebase.DbRef ref = firebase.database().ref().child("cities/1").as("city").
							root.child("countries").childViaVar("city", "country").as("country");
	addToLog(ref,"database().ref().child(\"cities/1\").as(\"city\").\nroot.child(\"countries\").childViaVar(\"city\", \"country\").as(\"country\")",{
		"DbRef.onValueCancelAll": () {
			ref.onValueCancelAll();
			return true;
		}
	});
//	ref.onComplexValue.listen((dynamic v) {
//		addToLog(ref,"onComplexValue: "+JSON.encode(v));
//	});

}

void rqNestedVariableRefs(x) {
	firebase.DbRef ref = firebase.database().ref().child("cities").childViaVar("city").as("city_rec").
							root.child("countries").childViaVar("city_rec", "country").as("country");
	addToLog(ref,"database().ref().child(\"cities\").childViaVar(\"city\").as(\"city_rec\").\nroot.child(\"countries\").childViaVar(\"city_rec\", \"country\").as(\"country\")",{
		"DbRef.onValueCancelAll": () {
			ref.onValueCancelAll();
			return true;
		},
		"DbRef.setVar(\"city\",\"1\")": () {
			ref.setVar("city","1");
		},
		"DbRef.setVar(\"city\",\"2\")": () {
			ref.setVar("city","2");
		},
		"DbRef.setVar(\"city\",\"3\")": () {
			ref.setVar("city","3");
		},
		"DbRef.setVar(\"city\",null)": () {
			ref.setVar("city",null);
		}
	});
//	ref.onComplexValue.listen((dynamic v) {
//		addToLog(ref,"onComplexValue: "+JSON.encode(v));
//	});

}